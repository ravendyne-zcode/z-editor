- source file
	- [x] create tab + editor
	- [x] editor holds metadata
		- [x] filename
		- [ ] isdirty?
	- [x] close editor
	- [x] save source

- settings
	- [x] load settings as json
	- [ ] save settings as json

- compiling
	- [x] run command and capture output
	- [x] compiler command line/options

- output
	- [x] use terminal



- zbuilder:
	- [x] _commands dict should contain dicts of form {cmd:..., echo:..., etc.etc.} so that only keys are 'ext1+ext2'

https://github.com/ASzc/java-configparser


- [ ] make zconf module handle interpolations
- [ ] use zconf module for parsing config files
- [ ] convert zconf module to use nez (http://nez-peg.github.io/getting-started.html, https://arxiv.org/pdf/1511.08307.pdf) for both python and javascript

- [ ] use codemirror as editor (https://github.com/codemirror/codemirror)


- [ ] unit tests: https://github.com/philsquared/Catch
