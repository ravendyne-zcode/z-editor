c.cmd=arm-none-eabi-gcc
cpp.cmd=arm-none-eabi-c++
cpp.axf.cmd=arm-none-eabi-c++

ar.cmd=arm-none-eabi-gcc-ar

elf2bin.cmd=arm-none-eabi-objcopy
size.cmd=arm-none-eabi-size



c.flags=-c -g -Os -Wall -Wextra -std=gnu11 -ffunction-sections -fdata-sections -MMD -flto -fno-fat-lto-objects

cpp.flags=-c -g3 -O0 -fno-common -Wall -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-rtti -fno-exceptions -mcpu=cortex-m3 -mthumb 

cpp.axf.flags=-nostdlib -mcpu=cortex-m3 -mthumb -Xlinker --gc-sections -Xlinker -print-memory-usage 

S.flags=-c -g -x assembler-with-cpp -flto


ar.flags=rcs


elf2bin.flags= -v -O binary 

ldflags="-L{zeeduino.library.path}" -larduino-sketch-stub -larduino-xc -larduino-xc-lpc13xx -llpc_chip_13xx  "{zeeduino.library.path}/cr_cpp_config.o" "{zeeduino.library.path}/crp.o"



.C -> .O
"{path}{c.cmd}" {c.flags} -mcpu={build.mcu} {includes} "{source_file}" -o "{object_file}"

.CPP -> .O
"{path}{cpp.cmd}" {cpp.flags} -mcpu={build.mcu} {includes} "{source_file}" -o "{object_file}"

.S -> .O
"{path}{c.cmd}" {S.flags} -mmcu={build.mcu} {includes} "{source_file}" -o "{object_file}"


AR
archive_file_path={build.path}/{archive_file}
"{path}{ar.cmd}" {ar.flags} "{archive_file_path}" "{object_file}"


* -> ELF
"{path}{cpp.axf.cmd}" {cpp.axf.flags} -mcpu={build.mcu} -T "{zeeduino.library.path}/lpc1347_arduino.ld" -o "{build.path}/{build.project_name}.elf" {object_files} {ldflags}

ELF -> BIN
"{path}{elf2bin.cmd}" {elf2bin.flags} "{build.path}/{build.project_name}.elf" "{build.path}/{build.project_name}.bin"


---------------------------------------------------------------------------------------------------------------------------------------
SOURCE:

http://www.nxp.com/products/microcontrollers-and-processors/arm-processors/lpc-mcus/software-tools/lpcopen-libraries-and-examples:LPC-OPEN-LIBRARIES

http://www.nxp.com/assets/downloads/data/en/software/lpcopen_2_05_lpcxpresso_nxp_lpcxpresso_1347.zip


LPC - valid image checksums
https://www.lpcware.com/content/faq/lpcxpresso/checksum-in-image
https://community.nxp.com/message/630662
