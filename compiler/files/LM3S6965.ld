GROUP (
  libgcc.a
  libstdc++.a
/*  libc.a */
  libc_nano.a
  libnosys.a

  libm.a

  crti.o
  crtn.o

  crtbegin.o
  crtend.o
)



/*
    LM3S6965 memory regions
*/
MEMORY
{
    MCUFlash (rx) :       ORIGIN = 0x0,         LENGTH = 0x40000  /* 256K bytes (aka Flash)  */
    MainRam (rwx) :       ORIGIN = 0x20000000,  LENGTH = 0x10000  /* 64K bytes (aka RAM)     */
    /* these are fake */
    USBRam (rwx) :        ORIGIN = 0x20004000,  LENGTH = 0x800    /* 2K bytes (aka RAM2)    */
    PeripheralRam (rwx) : ORIGIN = 0x30000000,  LENGTH = 0x800    /* 2K bytes (aka RAM3)    */
}

/* Symbols for base/top of each memory region */

__base_MCUFlash = 0x0  ; /* MCUFlash */  
__base_Flash = 0x0 ; /* Flash */  
__top_MCUFlash = 0x0 + 0x40000 ; /* 64K bytes */  
__top_Flash = 0x0 + 0x40000 ; /* 64K bytes */  

__base_MainRam = 0x20000000  ; /* MainRam */  
__base_RAM = 0x20000000 ; /* RAM */  
__top_MainRam = 0x20000000 + 0x10000 ; /* 8K bytes */  
__top_RAM = 0x20000000 + 0x10000 ; /* 8K bytes */  

__base_USBRam = 0x20004000  ; /* USBRam */  
__base_RAM2 = 0x20004000 ; /* RAM2 */  
__top_USBRam = 0x20004000 + 0x800 ; /* 2K bytes */  
__top_RAM2 = 0x20004000 + 0x800 ; /* 2K bytes */  

__base_PeripheralRam = 0x30000000  ; /* PeripheralRam */  
__base_RAM3 = 0x30000000 ; /* RAM3 */  
__top_PeripheralRam = 0x30000000 + 0x800 ; /* 2K bytes */  
__top_RAM3 = 0x30000000 + 0x800 ; /* 2K bytes */  




/**************************************************************************************
    Sections
*/

ENTRY(ResetISR)

SECTIONS
{
    /* MAIN TEXT SECTION */
    .text : ALIGN(4)
    {
        /* When erased, a flash memory is filled with 0xFF */
        FILL(0xFF)

        __vectors_start__ = ABSOLUTE(.);
        KEEP(*(.isr_vector))

        *(.after_vectors*)

        /* LPC Code Read Protect register */
        . = 0x000002FC ;
        PROVIDE(__CRP_WORD_START__ = .) ;
        KEEP(*(.crp))
        PROVIDE(__CRP_WORD_END__ = .) ;

        ASSERT(!(__CRP_WORD_START__ == __CRP_WORD_END__), "No CRP register value provided");
        /* End of LPC Code Read Protect register */

    } > MCUFlash

    .text : ALIGN(4)    
    {
        *(.text*)
        *(.rodata .rodata.* .constdata .constdata.*)

        . = ALIGN(4);

        /* C++ ctors */
        . = ALIGN(4);
        KEEP(*(.init))
        
        . = ALIGN(4);
        __preinit_array_start = .;
        KEEP (*(.preinit_array))
        __preinit_array_end = .;
        
        . = ALIGN(4);
        __init_array_start = .;
        KEEP (*(SORT(.init_array.*)))
        KEEP (*(.init_array))
        __init_array_end = .;
        
        /* C++ dtors */
        KEEP(*(.fini));
        
        . = ALIGN(4);
        KEEP (*crtbegin.o(.ctors))
        KEEP (*(EXCLUDE_FILE (*crtend.o) .ctors))
        KEEP (*(SORT(.ctors.*)))
        KEEP (*crtend.o(.ctors))
        
        . = ALIGN(4);
        KEEP (*crtbegin.o(.dtors))
        KEEP (*(EXCLUDE_FILE (*crtend.o) .dtors))
        KEEP (*(SORT(.dtors.*)))
        KEEP (*crtend.o(.dtors))
        . = ALIGN(4);
        /* End C++ */

    } > MCUFlash

    .ARM.extab : ALIGN(4) 
    {
        *(.ARM.extab* .gnu.linkonce.armextab.*)
    } > MCUFlash

    __exidx_start = .;
    .ARM.exidx : ALIGN(4)
    {
        *(.ARM.exidx* .gnu.linkonce.armexidx.*)
    } > MCUFlash
    __exidx_end = .;

    .copy.table : ALIGN(4)
    {
        /* Global Section Table */
        /*
            Create section table to help with
            .data and .bss initialization
            Here we have noncontiguous RAM space:
            RAM, USB RAM and peripheral RAM.
            We could have .data/.bss init function for each
            or we can have one function that loads its parameters
            from here and then initializes each of the RAM regions.
        */

        . = ALIGN(4) ; 
        __section_table_start = .;

        __data_section_table = .;
        LONG(LOADADDR(.data)); /* LMA of start of a section to copy from */
        LONG(    ADDR(.data)); /* VMA of start of a section to copy to */
        LONG(  SIZEOF(.data)); /* size of the section to copy */
        /* gcc.ld uses LONG (__data_end__ - __data_start__)
        we use SIZEOF() */
        LONG(LOADADDR(.data_RAM2));
        LONG(    ADDR(.data_RAM2));
        LONG(  SIZEOF(.data_RAM2));
        LONG(LOADADDR(.data_RAM3));
        LONG(    ADDR(.data_RAM3));
        LONG(  SIZEOF(.data_RAM3));
        __data_section_table_end = .;
        
        __bss_section_table = .;
        LONG(    ADDR(.bss));
        /* gcc.ld uses LONG (__bss_end__ - __bss_start__)
        we use SIZEOF() */
        LONG(  SIZEOF(.bss));
        LONG(    ADDR(.bss_RAM2));
        LONG(  SIZEOF(.bss_RAM2));
        LONG(    ADDR(.bss_RAM3));
        LONG(  SIZEOF(.bss_RAM3));
        __bss_section_table_end = .;
        
        __section_table_end = . ;
        /* End of Global Section Table */
    } > MCUFlash


    _etext = .;


    /* DATA section for USBRam */
    .data_RAM2 : ALIGN(4)
    {
        FILL(0xFF)
        PROVIDE(__start_data_RAM2 = .) ;
        *(.ramfunc.$RAM2)
        *(.ramfunc.$USBRam)
        *(.data.$RAM2*)
        *(.data.$USBRam*)
        . = ALIGN(4) ;
        PROVIDE(__end_data_RAM2 = .) ;
     } > USBRam AT>MCUFlash

    /* DATA section for PeripheralRam */
    .data_RAM3 : ALIGN(4)
    {
        FILL(0xFF)
        PROVIDE(__start_data_RAM3 = .) ;
        *(.ramfunc.$RAM3)
        *(.ramfunc.$PeripheralRam)
        *(.data.$RAM3*)
        *(.data.$PeripheralRam*)
        . = ALIGN(4) ;
        PROVIDE(__end_data_RAM3 = .) ;
     } > PeripheralRam AT>MCUFlash




    /* Main DATA section (MainRam) */
    .data : ALIGN(4)
    {
       FILL(0xFF)
       _data = . ;
       *(vtable)
       *(.ramfunc*)
       *(.data*)
       . = ALIGN(4) ;
       _edata = . ;
    } > MainRam AT>MCUFlash




    /* MAIN DATA SECTION  <--- Move before .data above???? */
    .uninit_RESERVED : ALIGN(4)
    {
        KEEP(*(.bss.$RESERVED*))
        . = ALIGN(4) ;
        _end_uninit_RESERVED = .;
    } > MainRam



    /* BSS section for USBRam */
    .bss_RAM2 : ALIGN(4)
    {
       PROVIDE(__start_bss_RAM2 = .) ;
       *(.bss.$RAM2*)
       *(.bss.$USBRam*)
       . = ALIGN (. != 0 ? 4 : 1) ; /* avoid empty segment */
       PROVIDE(__end_bss_RAM2 = .) ;
    } > USBRam 

    /* BSS section for PeripheralRam */
    .bss_RAM3 : ALIGN(4)
    {
       PROVIDE(__start_bss_RAM3 = .) ;
       *(.bss.$RAM3*)
       *(.bss.$PeripheralRam*)
       . = ALIGN (. != 0 ? 4 : 1) ; /* avoid empty segment */
       PROVIDE(__end_bss_RAM3 = .) ;
    } > PeripheralRam




    /* MAIN BSS SECTION */
    .bss : ALIGN(4)
    {
        _bss = .;
        *(.bss*)
        *(COMMON)
        . = ALIGN(4) ;
        _ebss = .;
        PROVIDE(end = .);
    } > MainRam



    /* NOINIT section for USBRam */
    .noinit_RAM2 (NOLOAD) : ALIGN(4)
    {
       *(.noinit.$RAM2*)
       *(.noinit.$USBRam*)
       . = ALIGN(4) ;
    } > USBRam 

    /* NOINIT section for PeripheralRam */
    .noinit_RAM3 (NOLOAD) : ALIGN(4)
    {
       *(.noinit.$RAM3*)
       *(.noinit.$PeripheralRam*)
       . = ALIGN(4) ;
    } > PeripheralRam 

    /* DEFAULT NOINIT SECTION */
    .noinit (NOLOAD): ALIGN(4)
    {
        _noinit = .;
        *(.noinit*) 
         . = ALIGN(4) ;
        _end_noinit = .;
    } > MainRam





    /*
        Override dafault stack and heap locations by
        providing your own values for
        __user_stack_top
        and
        __user_heap_base
    */
    PROVIDE(_heap_start = DEFINED(__user_heap_base) ? __user_heap_base : .);
    PROVIDE(_stack_top = DEFINED(__user_stack_top) ? __user_stack_top : __top_MainRam - 0);

    /* Create checksum value (used in startup) */
    PROVIDE(__user_code_checksum = 0 - (
                                      _stack_top 
                                       + (ResetISR + 1) 
                                       + (NMI_Handler + 1) 
                                       + (HardFault_Handler + 1) 
                                       + (MemManage_Handler + 1)
                                       + (BusFault_Handler + 1)
                                       + (UsageFault_Handler + 1)
                                      ));
}
