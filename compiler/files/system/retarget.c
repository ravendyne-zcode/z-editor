// share/gcc-arm-none-eabi/samples/src/retarget/retarget.c
// see share/gcc-arm-none-eabi/samples/src/retarget/Makefile
// for CFLAGS+=-fno-builtin
void retarget_init()
{
  // Initialize UART
}

volatile unsigned int * const UART0DR = (unsigned int *)0x4000c000;


int _write (int fd, char *ptr, int len)
{
  /* Write "len" of char from "ptr" to file id "fd"
   * Return number of char written.
   * Need implementing with UART here. */
  int siz = len;
 while(*ptr != '\0' && siz > 0) { /* Loop until end of string */
 *UART0DR = (unsigned int)(*ptr); /* Transmit char */
 ptr++; /* Next char */
  siz--;
 }
  return len - siz;
}

int _read (int fd, char *ptr, int len)
{
  /* Read "len" of char to "ptr" from file id "fd"
   * Return number of char read.
   * Need implementing with UART here. */
  return len;
}

void _ttywrch(int ch) {
  /* Write one char "ch" to the default console
   * Need implementing with UART here. */
}

/* SystemInit will be called before main */
void SystemInit()
{
    retarget_init();
}

