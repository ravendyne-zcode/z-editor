const electron = require('electron')
// IPC
ipcMain = electron.ipcMain

const path = require('path')
const fs = require('fs')

ipcMain.on('zeditor-file-service', (event, arg) => {
    console.log(arg)

    // TODO: paths are relative to main.js (application root folder in this case)
    var fullPath = path.join(path.dirname(require.main.filename), arg.path, arg.fileName)
    console.log('require.main.filename', require.main.filename)
    console.log('fullPath', fullPath)

    var returnValue =
    {
        status: false,
        cmd: arg.cmd,
        path: arg.path,
        fileName: arg.fileName,
        fullPath: fullPath,
        content: arg.content,
    }

    if(arg.cmd && arg.cmd == 'save')
    {
        try
        {
            fs.writeFileSync(fullPath, arg.content)

            returnValue.status = true
            
        }
        catch(ex)
        {
            returnValue.status = false
            returnValue.content = ex
        }
    }
    else if(arg.cmd && arg.cmd == 'load')
    {
        try
        {
            returnValue.content = fs.readFileSync(fullPath, 'utf8')

            returnValue.status = true
            
        }
        catch(ex)
        {
            returnValue.status = false
            returnValue.content = ex
        }
    }

    event.returnValue = returnValue
})
