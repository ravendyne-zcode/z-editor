const electron = require('electron')
// IPC
ipcMain = electron.ipcMain

const path = require('path')
const fs = require('fs')

const exec = require('child_process').exec

let syscmdSender = null


var testCmd = (sender, cmd) =>
{
    var child = exec(cmd)

    var returnValue =
    {
        status: false,
        done: false,
        cmd: cmd,
        content: '',
    }

    child.stdout.on('data', function(data) {
        console.log(data)
        sender.send('zeditor-syscmd-service-reply', {
            status: true,
            done: false,
            cmd: cmd,
            content: data,
        })
    })

    child.stderr.on('data', function(data) {
        console.log(data)
        sender.send('zeditor-syscmd-service-reply', {
            status: true,
            done: false,
            cmd: cmd,
            content: data,
        })
    })

    child.on('close', function() {
        sender.send('zeditor-syscmd-service-reply', {
            status: true,
            done: true,
            cmd: cmd,
            content: '',
        })
    })
}


ipcMain.on('zeditor-syscmd-service', (event, arg) => {
    console.log(arg)

    var returnValue =
    {
        status: false,
        done: false,
        cmd: arg.cmd,
        content: '',
    }

    returnValue.status = true
    returnValue.done = true
    returnValue.content = 'the command output chunk'

    // async
    syscmdSender = event.sender

    testCmd(syscmdSender, arg.cmd)

    // sync
    // event.returnValue = returnValue
})
