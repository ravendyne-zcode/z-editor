## Z-Editor: Web based IDE for Z-Code

Simple IDE designed to run both on a Web server as well as in an [Electron](https://electronjs.org/) based desktop app.
