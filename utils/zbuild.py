#!/usr/bin/python

import os
import sys
import string
import ConfigParser
import argparse

# from colored import fore, back, style

from cmdrunner import RunCommand, TextOutput


# https://docs.python.org/2/howto/argparse.html
parser = argparse.ArgumentParser(description='Simple build script.', epilog="GPL v2")
parser.add_argument('configFolder', help='Folder where build.config is. By default this is also working (source) folder.', nargs='?', default='.')
parser.add_argument('-s', '--source', help='Working (source) folder, FOLDERS list is resolved relative to this.', default=None)

parser.add_argument('-c', '--compile', help='Compile source files', action="store_true", default=False)
parser.add_argument('-a', '--archive', help='Archive object files', action="store_true", default=False)
parser.add_argument('-l', '--link', help='Link object files/libraries into executable', action="store_true", default=False)
parser.add_argument('-b', '--build-config', help="Use specific build configuration file", default="build.config")

parser.add_argument('-C', '--clean', help='Clean', action="store_true", default=False)
parser.add_argument('-r', '--run', help='Execute one of literal commands', default=None)

parser.add_argument('-v', '--verbose', help='Specify verbosity level', action="count", default=0)

args = parser.parse_args()


# we are sensitive about case :)
# subclassing ensures that if we specify DEFAULTS in parser constructor,
# options in there will also keep their case
class CaseConfigParser(ConfigParser.SafeConfigParser):
    def optionxform(self, optionstr):
        return optionstr


"""
    Used for RunCommand
"""
class MyTextOutput(TextOutput):
    def output_clear(self):
        pass

    def output_append(self, text='\n'):
        sys.stdout.write(text)
        sys.stdout.flush()


"""
    Helper functions
"""
def eprint(*args):
    for x in args:
        print >> sys.stderr, x,
    print >> sys.stderr, ''


def normalize_specification_string_and_convert_to_list(spec):
    """
        Split the string on newlines and then trim space from both sides
        converting it into a list in the process.
        After trimming, discard empty lines and lines starting with '#'.
    """
    _spec = map(string.strip, spec.splitlines())
    # remove empty lines from the list and lines starting with '#'
    _spec = [x for x in _spec if x != "" and not x.startswith('#')]

    return _spec

def add_trailing_slash(folder):
    return os.path.join(folder, '')

class BuildData:
    # TODO: consider doing this for 'constants' ??
    # http://code.activestate.com/recipes/65207-constants-in-python/
    CMD_ECHO_KEY = '.ECHO'
    CMD_ECHO_DEFAULT_VALUE = 'ECHO'

    def __init__(self, args):
        # the folder where we expect to find build.config
        self.configFolder = os.path.abspath(args.configFolder)
        # the root folder where source files are (given in FILES)
        self.baseFolder = self.configFolder if args.source == None else args.source
        self.baseFolder = os.path.abspath(self.baseFolder)
        # the folder where output files are placed, unless absolute path is specified for them
        self.targetFolder = os.getcwd()

        if args.verbose > 1:
            print 'args', args
            print 'baseFolder', self.baseFolder
            print 'targetFolder', self.targetFolder
            print 'configFolder', self.configFolder

        self.load_configuration(args)
        self.pre_process_settings(args)

    def config_get_items(self, section):
        if section in self.no_vars_sections:
            return self.Config.items(section)
        else:
            return self.Config.items(section, vars=self.config_vars)

    def config_get_option(self, section, option, default=None):
        try:
            if section in self.no_vars_sections:
                return self.Config.get(section, option)
            else:
                return self.Config.get(section, option, vars=self.config_vars)
        except ConfigParser.NoOptionError:
            if default:
                return default
            raise

    def load_configuration(self, args):
        # https://docs.python.org/2/library/configparser.html
        self.config_vars = {
            'CONFIG_FOLDER': add_trailing_slash(self.configFolder),
            'BASE_FOLDER': add_trailing_slash(self.baseFolder),
            'TARGET_FOLDER': add_trailing_slash(self.targetFolder),
        }
        self.no_vars_sections = ['map', 'reduce', 'collect-reduce']
        Config = CaseConfigParser()

        default_build_config = os.path.normpath(os.path.join(self.configFolder, 'build.config'))
        args_build_config = os.path.normpath(os.path.join(self.configFolder, args.build_config))
        if args.verbose > 1:
            print 'default_build_config', default_build_config
            print 'args_build_config', args_build_config
            print 'config vars', self.config_vars

        configs_read = Config.read([default_build_config, args_build_config])
        self.Config = Config
        
        if args.verbose > 2:
            print 'Config files parsed: ', configs_read
            print Config.sections()
            print self.config_get_items('map')
            print self.config_get_items('reduce')
            print self.config_get_items('collect-reduce')
            print self.config_get_items('transforms')

        #
        # load up sections/values we need
        #
        self.FOLDERS = self.config_get_option('params', 'FOLDERS')
        self.FILES = self.config_get_option('params', 'FILES')
        self.TARGET_NAME = self.config_get_option('params', 'TARGET_NAME')

        # gets src->dst mappings like '.cpp' -> '.o', '.c' -> '.o', '.o' -> '.a' etc.
        self.extension_mappings_map = self.config_get_items('map')
        self.extension_mappings_reduce = self.config_get_items('reduce')
        self.extension_mappings_collect_reduce = self.config_get_items('collect-reduce')

        self.literals = self.config_get_items('literals')


    def pre_process_settings(self, args):
        self.FOLDERS = normalize_specification_string_and_convert_to_list(self.FOLDERS)
        if args.verbose > 1:
            print 'FOLDERS', self.FOLDERS

        self.FILES = normalize_specification_string_and_convert_to_list(self.FILES)
        if args.verbose > 1:
            print 'FILES', self.FILES

        """
            Commands to translate items from list of files:

            for each item in list_of_files:

                item.ext1  ---------->  item.ext2
                            command
                   >$$                    $$>
        """
        self.commands_for_map = self.construct_commands_dictionary('transforms', self.extension_mappings_map)
        if args.verbose > 1:
            print 'commands_for_map', self.commands_for_map

        """
            Commands to translate items from list of files to one target:

            for each item in list_of_files:

                item.ext1  ---------->  target.ext2
                            command
                   >$$                    $$>

        """
        self.commands_for_reduce = self.construct_commands_dictionary('transforms', self.extension_mappings_reduce)
        if args.verbose > 1:
            print 'commands_for_reduce', self.commands_for_reduce

        """
            Commands to translate items from list of files to one target:

            - first collect all items in the list into one item (string)
            - then do command where the collection is source

                collection  ---------->  target.ext2
                             command
                   >$$                      $$>

        """
        self.commands_for_collect_reduce = self.construct_commands_dictionary('transforms', self.extension_mappings_collect_reduce)
        if args.verbose > 1:
            print 'commands_for_collect_reduce', self.commands_for_collect_reduce

        # now that we are done, convert tuple lists to dictionaries
        self.extension_mappings_map = { k: v for (k, v) in self.extension_mappings_map }
        self.extension_mappings_reduce = { k: v for (k, v) in self.extension_mappings_reduce }
        self.extension_mappings_collect_reduce = { k: v for (k, v) in self.extension_mappings_collect_reduce }

        # convert literal commands to dictionary
        self.literals = { k: normalize_specification_string_and_convert_to_list(v) for (k, v) in self.literals }


    """
        Translation (map) and accumulation (reduce) commands.

        (.ext1, .ext2) pairs are specified in [map] or [reduce] sections like this:
        
            .ext1: .ext2

        for example:
        [map]
            .cpp: .o
            .c: .o

        etc.

        Commands for both operations are specified in section [transforms] under keys in
        the form ".ext1.ext2", for example

        [transforms]
        .cpp.o:
            gcc
            -c
            -o
            $$>.o
            >$$.c

        .o.a:
            ar
            rcs
            lib$$>.a
            >$$.o
    """

    def construct_commands_dictionary(self, section_name, extension_pairs):
        # this step collects command strings from [transforms] section for each (.ext1, .ext2) pair into dictionary
        # where keys are '.ext1.ext2' and values are values of '.ext1.ext2' options from [transforms] section (a.k.a. command strings)
        _commands = { k+v: self.config_get_option(section_name, k + v) for (k, v) in extension_pairs }
        # this step converts command string into list using newline as delimiter, and then trims each list element and removes
        # the empty ones and those starting with '#'
        _commands = { k: normalize_specification_string_and_convert_to_list(v) for k, v in _commands.iteritems() }

        # collect ECHO config values for each src+dst combination
        # ECHO is used to print out in short version command being executed (i.e. as CC, AR, LINK, etc.)
        _commands_echo = { k+v+self.CMD_ECHO_KEY: self.config_get_option(section_name, k + v + self.CMD_ECHO_KEY, self.CMD_ECHO_DEFAULT_VALUE) for (k, v) in extension_pairs }

        # construct commands dictionary so that keys are 'ext1+ext2' and values are dictionaries of form
        # {cmd: ..., echo: ..., etc.etc.}
        _commands = { k: {'cmd': v, 'echo': _commands_echo[k+self.CMD_ECHO_KEY]} for k,v in _commands.iteritems() }

        if args.verbose > 1:
            print '_commands', _commands
        return _commands

    def cmd_echo(self, src_ext, dst_ext, cmd_map, default_echo='CMD'):
        cmd_key = src_ext + dst_ext

        if args.verbose > 2:
            print 'cmd_echo', default_echo, cmd_key
            print 'cmd_echo cmd_map[cmd_key] ', cmd_map[cmd_key]

        cmd_echo_value = cmd_map[cmd_key]['echo'] if cmd_key in cmd_map and cmd_map[cmd_key]['echo'] != self.CMD_ECHO_DEFAULT_VALUE else default_echo
        return cmd_echo_value





build_data = BuildData(args)
# build_data.baseFolder
# build_data.targetFolder

# build_data.FOLDERS
# build_data.FILES
# build_data.TARGET_NAME

# build_data.extension_mappings_map
# build_data.extension_mappings_reduce
# build_data.extension_mappings_collect_reduce

# build_data.Config

# build_data.commands_for_map
# build_data.commands_for_reduce
# build_data.commands_for_collect_reduce







def get_file_modification_time_stamp(filename):
    mtime = 0
    try:
        mtime = os.path.getmtime(filename)
    except OSError:
        pass
    # last_modified_date = datetime.fromtimestamp(mtime)
    return mtime

def needs_build(src, dst):
    sourceTime = get_file_modification_time_stamp(src)
    targetTime = get_file_modification_time_stamp(dst)
    return sourceTime > targetTime



def substitute_placeholders(x, src, src_ext, dst, dst_ext):
    basename, _ = os.path.split(src)
    r = x

    # first substitute longest patterns
    # @>$$ - use src as is
    r = r.replace('@>$$', src)
    # @$$> - use dst as is
    r = r.replace('@$$>', dst)

    # then substitute shortest patterns
    # @$$ - use the last segment of src, i.e. 'filename' in '/test/filename'
    r = r.replace('@$$', basename)
    # >$$ - use src as is, append src ext
    r = r.replace('>$$', src+src_ext)
    # $$> - use dst as is, append dst ext
    r = r.replace('$$>', dst+dst_ext)
    return r

def should_substitute_with_collection(x):
    return ('>$$' in x) or ('@>$$' in x)


def make_parent_folders(filename):
    folders = os.path.dirname(filename)
    if not os.path.exists(folders):
        os.makedirs(folders)


def locate_file(filename):
    """
        Search for given file through the list of FOLDERS
        from build config and return as soon as you find one.
        
        If no folder contains given file, default is to return
        None which means that file doesn't exist.
    """
    location = None

    for folder in build_data.FOLDERS:
        check_location = os.path.normpath(os.path.join(build_data.baseFolder, folder, filename))
        if os.path.exists(check_location):
            location = check_location
            break

    return location


def execute_build_command(cmd, builditem, verbose):
    if verbose > 1:
        print ' '
        print ' '.join(cmd)
    t = RunCommand(cmd, MyTextOutput())
    # in the distant future we can have parallel builds
    # for now, do it one by one
    t.join()

def replace_extension(fileitem, new_extension):
    """
        If fileitem has extension, it will be replaced with new_extension.
        If fileitem doesn't have extension, it will get new_extension.
    """
    fileitem, _ = os.path.splitext(fileitem)
    return fileitem + new_extension



def get_mapped_extension(fileitem, extension_map):
    _, src_extension = os.path.splitext(fileitem)
    dst_extension = extension_map[src_extension]
    return dst_extension


def get_source(fileitem, extension_map):
    """
        Determine source path and extension.
        Uses baseFolder.

        Determines absolute path for fileitem
        and makes sure the file exists.
        Makes sure returned source extension is lowercase.
        Makes sure source extension is mapped to a transform command.
    """
    # get the full path of the source item if it exists in one of the FOLDERS
    sourcefile = locate_file(fileitem)
    if sourcefile == None:
        eprint ("File %s can't be found in any of the FOLDERS. Skipping..."%(fileitem))
        return None

    # get base file names (with no extension) for source and destination
    sourcefile, src_extension = os.path.splitext(sourcefile)
    src_extension = src_extension.lower()
    if not src_extension in extension_map.keys():
        eprint ('Unknown file type for %s'%(fileitem))
        return None

    return (sourcefile, src_extension)


def get_destination(fileitem, target, extension_map):
    """
        Determine destination path and extension.
        Uses targetFolder.

        Makes sure returned destination extension is lowercase.
        Makes sure destination extension has it's src->dst mapping.
        Makes sure given source's extension is mapped to the destination's extension.
        Makes sure folder(s) exist where destination file should be placed, creates
        them if they don't.
    """
    destinationfile, dst_extension = os.path.splitext(os.path.normpath(os.path.join(build_data.targetFolder, target)))
    dst_extension = dst_extension.lower()

    if not dst_extension in extension_map.values():
        eprint ('Unknown file type for %s'%(target))
        return
    if dst_extension != get_mapped_extension(fileitem, extension_map):
        eprint ("No rule to build %s with extension '%s'"%(target, dst_extension))
        return None

    # make sure output folder exists
    make_parent_folders(destinationfile + dst_extension)

    return (destinationfile, dst_extension)


def get_source_destination(fileitem, target, extension_map):
    src = get_source(fileitem, extension_map)
    dst = get_destination(fileitem, target, extension_map)

    if src == None or dst == None:
        return None

    sourcefile, src_extension = src
    destinationfile, dst_extension = dst

    return (sourcefile, src_extension, destinationfile, dst_extension)


def get_command(fileitem, target, commands_map, extension_map):
    # make sure source has its target mapping
    _, src_extension = os.path.splitext(fileitem)
    _, dst_extension = os.path.splitext(target)

    if not src_extension in extension_map:
        eprint("Source '%s' has no mapping to destination"%(fileitem))
        return None

    if extension_map[src_extension] != dst_extension:
        eprint("There is no rule to map '%s' to '%s'"%(fileitem, target))
        return None

    if not src_extension+dst_extension in commands_map.keys():
        eprint("There is no command specified for transforming '%s' to '%s'"%(src_extension, dst_extension))
        return None

    return commands_map[src_extension+dst_extension]['cmd']


def transform_map(fileitem):
    #
    # get source and destination and
    # make sure source file and destination folder exists
    #
    dst_extension = get_mapped_extension(fileitem, build_data.extension_mappings_map)
    target = replace_extension(fileitem, dst_extension)

    source_destination = get_source_destination(fileitem, target, build_data.extension_mappings_map)
    if source_destination == None:
        return
    
    # unpack data
    sourcefile, src_extension, destinationfile, dst_extension = source_destination

    #
    # skip if doesn't need build
    #
    if not needs_build(sourcefile+src_extension, destinationfile+dst_extension):
        print 'SKIP: ' + fileitem
        return


    #
    # prepare build command
    #
    # perform $$ substitutions in 'map' command string
    command_parts = build_data.commands_for_map[src_extension+dst_extension]['cmd']
    cmd = [substitute_placeholders(x, sourcefile, src_extension, destinationfile, dst_extension) for x in command_parts]


    #
    # execute build command
    #
    if args.verbose:
        print build_data.cmd_echo(src_extension, dst_extension, build_data.commands_for_map, 'MAP') + ': ' + fileitem
    execute_build_command(cmd, fileitem, args.verbose)




def transform_reduce(fileitem, target):
    #
    # get source and destination and
    # make sure source file and destination folder exists
    #
    source_destination = get_source_destination(fileitem, target, build_data.extension_mappings_reduce)
    if source_destination == None:
        return
    
    # unpack data
    sourcefile, src_extension, destinationfile, dst_extension = source_destination

    #
    # prepare build command
    #
    # perform $$ substitutions in 'reduce' command string
    # command_parts = build_data.commands_for_reduce[src_extension+dst_extension]
    command_parts = get_command(fileitem, target, build_data.commands_for_reduce, build_data.extension_mappings_reduce)
    if command_parts == None:
        return

    cmd = [substitute_placeholders(x, sourcefile, src_extension, destinationfile, dst_extension) for x in command_parts]

    if args.verbose:
        print build_data.cmd_echo(src_extension, dst_extension, build_data.commands_for_reduce, 'REDUCE') + ': ' + fileitem
    execute_build_command(cmd, fileitem, args.verbose)


def transform_collect_reduce(target):
    #
    # determine source path and extension
    #
    # TODO: get this from build.config
    src_extension = '.o'
    
    # collect all the source items, without extensions, from FILES
    src_collection = []
    for fileitem in build_data.FILES:
        fileitem = replace_extension(fileitem, src_extension)

        src = get_source(fileitem, build_data.extension_mappings_collect_reduce)
        if src == None:
            return
        sourcefile, src_extension = src

        src_collection.append(sourcefile + src_extension)

    #
    # determine destination path and extension
    #
    # TODO: remove the dummy here?
    dst = get_destination('dummy'+src_extension, target, build_data.extension_mappings_collect_reduce)
    if dst == None:
        return
    destinationfile, dst_extension = dst


    #
    # prepare build command
    #

    # at this point *_commands[] dictionaries contain values which are lists
    # i.e. every command ('transforms') string from config file has been split by newlines
    # into a list of values
    # 'collect-reduce' is different since it has as a source a list of items as a single collection.
    # this means that '>$$' and '@>$$' and any other source placeholder has to be replaced with
    # a COLLECTION of items, and not just with ONE item, as is the case with 'map' and 'reduce'.
    # so, first thing to do is to find every item of the collect_reduce_command which contains
    # SOURCE placeholder, and replace it with a COLLECTION of source (input) items
    # command_parts = build_data.commands_for_collect_reduce[src_extension+dst_extension]
    command_parts = get_command(fileitem, target, build_data.commands_for_collect_reduce, build_data.extension_mappings_collect_reduce)
    if command_parts == None:
        return

    # process source substitution
    res = [[x] if not should_substitute_with_collection(x) else src_collection for x in command_parts]
    # flatten a list of lists
    # think of it as nested for loops which yield item
    # for sublist in res: for item in sublist: yield item
    command_parts = [item for sublist in res for item in sublist]
    
    # process destination substitution
    # we supply sourcefile parameter here to be the baseFolder
    # so we can use @>$$ placeholder for command paramters (i.e. linker scripts etc.)
    cmd = [substitute_placeholders(x, build_data.baseFolder, '', destinationfile, dst_extension) for x in command_parts]

    #
    # execute build command
    #
    if args.verbose:
        print build_data.cmd_echo(src_extension, dst_extension, build_data.commands_for_collect_reduce, 'COLLECT_REDUCE') + ': ' + target
    execute_build_command(cmd, target, args.verbose)




if args.run:
    literals = build_data.literals
    run_literal = literals.get(args.run,[])
    for cmd in run_literal:
        if args.verbose:
            print 'RUN: ' + cmd
        execute_build_command(['bash', '-c', cmd], args.run, args.verbose)
    quit(0)

if args.clean:
    literals = build_data.literals
    clean_cmd = literals.get('clean',[])
    for cmd in clean_cmd:
        if args.verbose:
            print 'CLEAN: ' + cmd
        execute_build_command(['bash', '-c', cmd], 'clean', args.verbose)
    quit(0)


if args.compile:
    for fileitem in build_data.FILES:
        transform_map(fileitem)

if args.archive:
    # unless specified, assume CWD is our source folder for archiving
    build_data.baseFolder = os.getcwd()

    # TODO: don't do it for ALL src extensions,
    # only for the ones that match target extension???
    for src_extension in build_data.extension_mappings_reduce.keys():
        for fileitem in build_data.FILES:
            fileitem = replace_extension(fileitem, src_extension)
            transform_reduce(fileitem, build_data.TARGET_NAME)

if args.link:
    # unless specified, assume CWD is our source folder for archiving
    build_data.baseFolder = os.getcwd()

    transform_collect_reduce(build_data.TARGET_NAME)

