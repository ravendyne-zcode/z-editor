#!/usr/bin/python

import web
import os
import sys

urls = (
    '/(.*)', 'hello'
)
app = web.application(urls, globals())

class hello:        
    def GET(self, name):
        if not name: 
            name = 'World'
        return 'Hello, ' + name + '!'

if __name__ == "__main__":
    # a complicated ways of specifying address:port for web.py
    # os.environ['PORT'] = '8000'
    if(len(sys.argv) > 1):
        sys.argv[1] = '127.0.0.1:8000'
    else:
        sys.argv.append('127.0.0.1:8000')
    app.run()

