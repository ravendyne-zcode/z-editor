#!/usr/bin/python

import sys
import os
import time

import random
import string
import json
from collections import deque

import cherrypy

# print os.environ#['PYTHONPATH']
# quit(0)

from cmdrunner import RunCommand, TextOutput
from build import ZEditorBuilder
from files import ZEditorFiles

class MyTextOutput(TextOutput):
    def output_clear(self):
        pass

    def output_append(self, text='\n'):
        sys.stdout.write(text)
        sys.stdout.flush()

# t = RunCommand(["python", "blah.py"], MyTextOutput())
# t.join()

class ZEditorBackend(object):
    @cherrypy.expose
    def index(self):
        pass


class ZEditorBuilderApp(object):
    def __init__(self):
        self.zbuilder = ZEditorBuilder()

    @cherrypy.expose
    def index(self, cmd='', params=''):
        result = self.zbuilder.handleRequest(cmd=cmd, params=params)
        for k,v in result['headers'].items():
            cherrypy.response.headers[k] = v

        return result['message']


class ZEditorFilesApp(object):
    def __init__(self, rootFolder):
        self.rootFolder = rootFolder

    @cherrypy.expose
    def index(self, cmd='', path='', fileName='', content=''):
        result = ZEditorFiles(self.rootFolder).handleRequest(cmd, path, fileName, content)
        cherrypy.response.headers['Content-Type'] = result['content-type']
        return result['content']



cwd = os.getcwd()
root_folder = os.path.normpath(os.path.join(os.path.realpath(os.path.abspath(os.getcwd())), '..'))
print 'root_folder', root_folder

# almost
# quit(0)
# http://stackoverflow.com/a/3991899

config = {'/':
    {
        'tools.staticdir.dir': root_folder,
    }
}
cherrypy.config.update('cherry.conf')

cherrypy.tree.mount(ZEditorBackend(), '/', 'cherry.app.conf')
cherrypy.tree.mount(ZEditorFilesApp(root_folder), '/files')
cherrypy.tree.mount(ZEditorBuilderApp(), '/builder')

# cherrypy.quickstart(ZEditorBackend(), '/')

cherrypy.engine.start()
cherrypy.engine.block()
