#!/usr/bin/python

# import SimpleHTTPServer
import CGIHTTPServer
import BaseHTTPServer

import argparse

parser = argparse.ArgumentParser(description='SimpleHTTPServer for which you can specify IP address to bind to.')
parser.add_argument('-b,', '--bind', help='ip address to bind to, i.e. 192.168.0.25', default='127.0.0.1')
parser.add_argument('-p,', '--port', help='port to use, i.e. 8090', type=int, default=8000)
parser.add_argument('-f', '--prefix', help='pretend as if serving from prefix', dest='url_prefix', default='zeddy')
parser.set_defaults(index_override=True)

args = parser.parse_args()

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

class MyRequestHandler(CGIHTTPServer.CGIHTTPRequestHandler):
    def do_GET(self):
        self.path = remove_prefix(self.path, '/{0}'.format(args.url_prefix))

        CGIHTTPServer.CGIHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        self.path = remove_prefix(self.path, '/{0}'.format(args.url_prefix))

        CGIHTTPServer.CGIHTTPRequestHandler.do_POST(self)


PORT = args.port
IPADDR = args.bind
httpd = BaseHTTPServer.HTTPServer((IPADDR, PORT), MyRequestHandler)

print "Serving at http://{0}:{1}/".format(IPADDR, PORT)

httpd.serve_forever()
