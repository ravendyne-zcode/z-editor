#!/usr/bin/env python

import cgi
import json
import os
import sys

import urllib2

import time
import random
import string
from collections import deque

from cmdrunner import RunCommand, TextOutput

class MyTextOutput(TextOutput):
    def __init__(self, queue):
        self.queue = queue

    def output_clear(self):
        self.queue.clear()
        print 'queue->clear()'
        sys.stdout.flush()

    def output_append(self, text='\n'):
        self.queue.append(text)
        print text,
        sys.stdout.flush()



# remove leading slash/backslash, if any
def removeLeadingSlash(path):
    if path[0:1] == os.sep:
        path = path[1:]
    return path

def urlencode(s):
    return urllib2.quote(s)

def urldecode(s):
    return urllib2.unquote(s).decode('utf8')


# http://stackoverflow.com/questions/16316966/python-cgi-script-displays-output-in-the-browser-only-after-its-completion
# https://github.com/byjg/jquery-sse
def encodeMessage(id, message):
    data = json.dumps(
        {
            'running': True,
            'output': message,
        }
    )
    result = 'id: {0}\n'.format(id)
    result += 'data: {0}\n'.format(message)
    result += '\n'
    return result


class ZEditorBuilder:
    def __init__(self):
        self.messages = deque()
        self.t = None
        self.messageId = 0

    def _generate(self):
        result['headers']['Content-Type'] = 'application/json'
        length = 8
        self.messages.clear()
        for i in range(7):
            message = ''.join(random.sample(string.hexdigits, int(length)))
            self.messages.append(message)

        result['message'] = json.dumps(
            {
                'running': False,
                'started': True,
                'output': ', '.join(self.messages),
            }
        )

    def _nextMessageId(self):
        self.messageId += 1
        return str(self.messageId)

    def _popAllMessages(self):
        message = ''
        while len(self.messages) > 0:
            message += self.messages.popleft()
        lines = message.splitlines()
        message = ''
        for line in lines:
            message += encodeMessage(self._nextMessageId(), line)
        return message

    def handleRequest(self, cmd, params):
        print 'params', params
        params = list(map(urldecode, params))

        result = {
            'headers': {},
            'message': '',
        }

        print 'cmd = ', cmd
        print 'self.t = ', self.t

        if cmd == 'poll':
            result['headers']['Content-Type'] = 'text/event-stream'
            result['headers']['Cache-Control'] = 'no-cache'
            print 'poll: messages->len() = ', len(self.messages)

            if not self.t.is_alive():
                message = self._popAllMessages()
                result['message'] = message + encodeMessage('CLOSE', 'done.')
                self.t = None
            else:
                result['message'] = self._popAllMessages()

        elif cmd == 'run':
            # self._generate()
            result['headers']['Content-Type'] = 'application/json'

            if self.t == None:
                # self.t = RunCommand(["ls", "-al"], MyTextOutput(self.messages))
                # self.t = RunCommand(["ping", "-n", "3", "google.com"], MyTextOutput(self.messages))
                self.t = RunCommand(params, MyTextOutput(self.messages))

                result['message'] = json.dumps(
                    {
                        'running': False,
                        'started': True,
                        'output': '"{0}" started'.format(' '.join(params)),
                    }
                )
            else:
                result['message'] = json.dumps(
                    {
                        'running': True,
                        'started': False,
                        'output': '',
                    }
                )

        elif cmd == 'build':
            result['headers']['Content-Type'] = 'application/json'

            result['message'] = json.dumps(
                {
                    'running': False,
                    'started': True,
                    'output': 'building "{0}"'.format(' '.join(params)),
                }
            )

        # print 'return: messages->len() = ', len(self.messages)
        return result



if __name__ == '__main__':
    # gets GET/POST parameters
    form = cgi.FieldStorage()

    cmd = form.getvalue('cmd')
    if not cmd:
        cmd = 'poll'

    buildParams = form.getvalue('params') or ''

    result = ZEditorBuilder().handleRequest(cmd=cmd, params=buildParams)

    for k,v in result['headers']:
        print "%s: %s\n".format(k, v)
    print

    print result['message']
