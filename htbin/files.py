#!/usr/bin/env python

import cgi
import json
import os

import urllib2

# remove leading slash/backslash, if any
def removeLeadingSlash(path):
    if path[0:1] == os.sep:
        path = path[1:]
    return path

def urlencode(s):
    return urllib2.quote(s)

def urldecode(s):
    return urllib2.unquote(s).decode('utf8')


class ZEditorFiles:
    # rootFolder = os.getcwd()

    def __init__(self, rootFolder):
        self.rootFolder = rootFolder

    def handleRequest(self, cmd, path, fileName, content = ''):
        # path.normpath() also converts '/' to '\' on Windows
        path = os.path.normpath(urldecode(path))
        fileName = os.path.normpath(urldecode(fileName))

        path = removeLeadingSlash(path)

        fileName = removeLeadingSlash(fileName)

        # cwd = os.getcwd()
        fullPath = os.path.normpath(os.path.join(self.rootFolder, path, fileName))

        fileExists = os.path.exists(fullPath)
        folderExists = os.path.exists(os.path.dirname(fullPath))

        output = {
            'content-type': '',
            'content': '',
        }

        if cmd == 'stat':
            output['content-type'] = 'application/json'
            output['content'] = json.dumps(
                {
                    'status': True,
                    'cmd': cmd,
                    'path': path,
                    'fileName': fileName,
                    'fullPath': fullPath,
                    'exists': fileExists,
                }
            )

        elif cmd == 'load':
            text = "<file doesn't exist>"
            if fileExists:
                with open(fullPath, 'rb') as f:
                    data = f.read()
                    text = data.decode('utf-8')

            output['content-type'] = 'text/plain'
            output['content'] = text

        elif cmd == 'save':
            if folderExists:
                with open(fullPath, 'wb') as f:
                    f.write(content.encode('utf-8'))

            output['content-type'] = 'application/json'
            output['content'] = json.dumps(
                {
                    'status': True,
                    'cmd': cmd,
                    'path': path,
                    'fileName': fileName,
                    'fullPath': fullPath,
                    'content': content,
                }
            )

        return output

if __name__ == '__main__':
    # gets GET/POST parameters
    form = cgi.FieldStorage()

    cmd = form.getvalue('cmd')
    if not cmd:
        cmd = 'stat'

    pathParam = form.getvalue('path') or ''
    fileNameParam = form.getvalue('fileName') or ''
    fileContentParam = form.getvalue('content') or ''

    rootFolder = os.getcwd()

    result = ZEditorFiles(rootFolder).handleRequest(cmd=cmd, path=pathParam, fileName=fileNameParam, content=fileContentParam)
    print 'Content-Type: %s'.format(result['content-type'])
    print
    print result['content']
