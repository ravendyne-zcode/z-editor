#!/usr/bin/python

import sys
import os

from subprocess import Popen, PIPE, STDOUT
from select import select
from threading import Thread
from Queue import Queue


class TextOutput:
    def output_clear(self):
        pass

    def output_append(self, text='\n'):
        pass


# from plumbum/lib.py, see https://github.com/tomerfiliba/plumbum
def read_fd_decode_safely(fd, size=4096):
    """
    This reads a utf-8 file descriptor and returns a chunck, growing up to
    three bytes if needed to decode the character at the end.
    Returns the data and the decoded text.
    """
    data = os.read(fd.fileno(), size)
    for i in range(4):
        try:
            return data, data.decode("utf-8")
        except UnicodeDecodeError as e:
            if e.reason != 'unexpected end of data':
                raise
            if i == 3:
                raise
            data += os.read(fd.fileno(), 1)


class CmdThread(Thread):
    def __init__(self, cmd, stdoutQ, stderrQ):
        Thread.__init__(self)
        self.cmd = cmd
        self.stdoutQ = stdoutQ
        self.stderrQ = stderrQ
        self.returncode = None

    def run(self):
        p = Popen(self.cmd, stdin=None, stdout=PIPE, stderr=STDOUT)
        # p = Popen(self.cmd, stdin=None, stdout=PIPE)
        # p = Popen(self.cmd, stdin=None, stdout=PIPE, stderr=PIPE)

        out = p.stdout
        err = p.stderr
        queues = {out: self.stdoutQ, err: self.stderrQ}
        while p.poll() is None:
            # data = os.read(out.fileno(), 1)
            # data = out.read(1)
            data, __ = read_fd_decode_safely(out, 4096)
            if not data:
                continue
            else:
                self.stdoutQ.put(data)

            # data, text = read_fd_decode_safely(err, 4096)
            # if not data:
            #     continue
            # else:
            #     self.stderrQ.put(text)

        self.returncode = p.returncode


def AppendText(text, textoutput):
    textoutput.output_append(text)

def CmdRunner(cmd, textoutput):
    my_stdout = Queue()
    my_stderr = Queue()
    t = CmdThread(cmd, my_stdout, my_stderr)
    t.start()
    while t.isAlive():
        if not my_stdout.empty():
            text = my_stdout.get()
            my_stdout.task_done()
            # sys.stdout.write(text)
            # sys.stdout.flush()
            if textoutput:
                AppendText(text, textoutput)
        # if not my_stderr.empty():
        #     text = my_stderr.get()
        #     my_stderr.task_done()
        #     sys.stderr.write(text)
        #     sys.stderr.flush()
        #     if textoutput:
        #         AppendText(text, textoutput)
        continue


def RunCommand(cmd, textoutput=None):
    t = Thread(target=CmdRunner, args=(cmd, textoutput))
    t.start()
    return t


if __name__ == '__main__':
    t = RunCommand(["python", "blah.py"])
    t.join()
