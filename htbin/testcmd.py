#!/usr/bin/python

import sys

from cmdrunner import RunCommand, TextOutput



class MyTextOutput(TextOutput):
    def output_clear(self):
        pass

    def output_append(self, text='\n'):
        sys.stdout.write(text)
        sys.stdout.flush()

t = RunCommand(["python", "blah.py"], MyTextOutput())
t.join()
