'use strict'

define(['module', 'jquery'], function(module, $j){

    var self = this
    var ZServiceElectron = {}
    self.settings = {}


    var initialize = function( config )
    {
        $j.extend(true, self.settings, config || {})
    }

    var extendParent = function( parent, config )
    {
        var myconfig = {}
        $j.extend(true, myconfig, self.settings)

        $j.extend(true, parent, ZServiceElectron)
        $j.extend(true, self.settings, config || {})
        $j.extend(true, self.settings, myconfig)
    }

    var loadFile = function( fileName, loadFromFilesLocation = true )
    {
        var electron = requirenode('electron')

        var fileLocation = 
            loadFromFilesLocation ?
            self.settings.filesLocation :
            ''
console.log('fileLocation <- ', fileLocation)
        var params = {
            cmd: 'load',
            path: fileLocation,
            fileName: fileName,
        }

        var response = electron.ipcRenderer.sendSync('zeditor-file-service', params)
        /*
            In case of fs error, this is example of response pobject
            {
            code: "ENOENT"
            errno: -2
            path: "/projects/test.zs"
            syscall: "open"
            }
        */
        console.log(response)
        
        return $j.Deferred().resolve(response.content).promise()
    }

    var saveFile = function( fileName, content, saveToFilesLocation = true )
    {
        var electron = requirenode('electron')

        var fileLocation = 
            saveToFilesLocation ?
            self.settings.filesLocation :
            ''

        var params = {
            cmd: 'save',
            path: fileLocation,
            fileName: fileName,
            content: content,
        }

        var response = electron.ipcRenderer.sendSync('zeditor-file-service', params)
        /*
            In case of fs error, this is example of response pobject
            {
            code: "ENOENT"
            errno: -2
            path: "/projects/test.zs"
            syscall: "open"
            }
        */
        console.log(response)
        
        return $j.Deferred().resolve(response).promise()
    }

    var runCommand = function( command, terminalOutput )
    {
        var electron = requirenode('electron')

        electron.ipcRenderer.on('zeditor-syscmd-service-reply', (event, arg) => {
            terminalOutput.echo(arg.content)
            // console.log(arg.content)
        })
        electron.ipcRenderer.send('zeditor-syscmd-service', {cmd: command.join(' ')})

        return $j.Deferred().resolve({}).promise()
    }

    var buildProject = function( project, terminalOutput )
    {
        return $j.Deferred().resolve({}).promise()
    }

    initialize(module.config())
    // public functions interface
    $j.extend(true, ZServiceElectron, {
        extend: extendParent,
        loadFile: loadFile,
        saveFile: saveFile,
        runCommand: runCommand,
        buildProject: buildProject,
    })

    return ZServiceElectron

})
