'use strict'

define(['module', 'jquery'], function(module, $j){

    var self = this
    var ZServiceWeb = {}
    self.settings = {}

    var initialize = function( config )
    {
        $j.extend(true, self.settings, config || {})
    }

    var extendParent = function( parent, config )
    {
        var myconfig = {}
        $j.extend(true, myconfig, self.settings)

        $j.extend(true, parent, ZServiceWeb)
        $j.extend(true, self.settings, config || {})
        $j.extend(true, self.settings, myconfig)
    }

    var loadFile = function( fileName, loadFromFilesLocation = true )
    {
        var fileLocation = 
            loadFromFilesLocation ?
            self.settings.filesLocation :
            ''

        var params = {
            cmd: 'load',
            path: fileLocation,
            fileName: fileName,
        }

        var $jqXHR = $j.get({url: self.settings.apiUrl + '?' + $j.param(params), dataType: 'text'})
        
        return $jqXHR
    }

    var saveFile = function( fileName, content, saveToFilesLocation = true )
    {
        var fileLocation = 
            saveToFilesLocation ?
            self.settings.filesLocation :
            ''

        var params = {
            cmd: 'save',
            path: fileLocation,
            fileName: fileName,
            content: content,
        }

        var $jqXHR = $j.post(self.settings.apiUrl, params)
        
        return $jqXHR
    }

    var runCommand = function( command, terminalOutput )
    {
        var commandParameter = command.map(function(seg){ return encodeURIComponent(seg) })
        commandParameter = commandParameter.join(' ')

        // cherrypy requires us to use traditional style of URL encoding arrays
        // so we use $j.param({}, true) form
        $j.get('/builder?cmd=run&' + $j.param({params: command}, true))
        .done(function(){
            var sse = $j.SSE('/builder?cmd=poll', {
                onMessage: function(e){ 
                    console.log('id: ', e.lastEventId)
                    console.log('data: ', e.data)
                    terminalOutput.echo(e.data)

                    if(e.lastEventId == 'CLOSE') sse.stop()
                },
                onEnd: function(e){
                    // e is undefined here?
                    console.log("End");
                },
            });
            /*
            To cancel a stream from the server, 
            respond with a non "text/event-stream" Content-Type 
            or return an HTTP status other than 200 OK (e.g. 404 Not Found).
            */
            sse.start();

        })

        return $j.Deferred().resolve({}).promise()
    }

    var buildProject = function( project, terminalOutput )
    {
        return $j.Deferred().resolve({}).promise()
    }

    initialize(module.config())
    // public functions interface
    $j.extend(true, ZServiceWeb, {
        extend: extendParent,
        loadFile: loadFile,
        saveFile: saveFile,
        runCommand: runCommand,
        buildProject: buildProject,
    })

    return ZServiceWeb

})
