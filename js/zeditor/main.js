define(['jquery',
    'zeditor/ZUtil', 'zeditor/ZService', 'zeditor/ZConfig', 'zeditor/ZEditor',
    'velocityui', 'jquerysse'], function($j, ZUtil, ZService, ZConfig, ZEditor) {
    $j(document).ready(function(){

        var initializeEditorTabs = function()
        {
            var settignsPanel = $j('#settings_tab').html()
            ZEditor.newTab('Settings', settignsPanel)
            
            ZEditor.newTab('blah2', '<p style="color: white;">blah</p>')

            ZEditor.openFile('test/arduinoSample.zs')
            .then(
                ZEditor.openFile('test/c_cpp_sample.cpp')
            )
        }

        var playground = function()
        {
            $j('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover',
            })

            $j('.close-tab').click(function(){
                console.log('close tab clicked')
            })

            $j('#test1').click(function(){
                testMe()
            })

            $j('#btnSave').click(function(){
                ZEditor.saveFile('test.zs', 'this is file content\nwith new lines and stuff')
                .done(function(){
                    console.log('saved')
                })
                .fail(function(e){
                    console.log(e)
                })
            })

            $j('#test2').click(function(){
                // ZEditor.newTab('test', '<p style="color: white;">TEST!</p>')
                var output = ZEditor.terminalOutput()
                output.clear()

                ZService.runCommand(['ping', '-n', '3', 'google.com'], output)
            })

            var output = ZEditor.terminalOutput()
            $j('#btnNew').click(function(){
                output.clear()
                output.echo('hola!')
            })
            $j('#btnOpen').click(function(){
                output.echo('more...')
            })

            $j('#btnVerify').click(function(){
                require(['zeditor/ZBuilder'], function(builder){
                    builder.build()
                })
            })

        }

        var testMe = function()
        {
            // ZService.loadFile('fragments.html')
            //     .done(function(content){
            //         console.log('content', content)
            //     })

            // ZConfig.loadConfig('config/build')
            ZConfig.loadConfig('config/ide')
            .done(function(conf){
                console.log('conf', conf)
            })
            .fail(function(message){
                console.log('failed', message)
            })
        }

        $j(document).ready(function(){
            console.log('running in electron: ', ZUtil.isInsideElectron())

            initializeEditorTabs()

            playground()
        })
    })
})
