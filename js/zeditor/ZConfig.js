'use strict'

define(['module', 'jquery', 'zeditor/ZUtil', 'zeditor/ZService'], function(module, $j, ZUtil, ZService){

    var ZConfig = {}
    var settings =
    {
        configEvalTemplate: "(function (config) { {0}; return config; }({}))",
    }

    var initialize = function( config )
    {
        $j.extend(true, settings, config || {})
    }

    var loadConfig = function(configFileName, loadFromFilesLocation=true)
    {
        var dfd = $j.Deferred()

        configFileName = configFileName + '.zconf'

        ZService.loadFile(configFileName, loadFromFilesLocation)
        .done(function(confFile){
            try
            {
                confFile = ZUtil.formatString(settings.configEvalTemplate, confFile)
                var confObject = eval(confFile)
                dfd.resolve(confObject)
            }
            catch(error)
            {
                var message = "Couldn't parse config '" + configFileName + "' -> " + error
                dfd.reject(message)
            }
        })

        return dfd.promise()
    }

    initialize(module.config())
    // public functions interface
    $j.extend(true, ZConfig, {
        loadConfig: loadConfig,
    })

    return ZConfig

})
