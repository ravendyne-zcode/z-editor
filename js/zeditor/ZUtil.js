'use strict'

define(['module', 'jquery'], function(module, $j){

    var ZUtil = {}
    var settings =
    {
    }

    var initialize = function( config )
    {
        $j.extend(true, settings, config || {})
    }

    var populateSelectOptions = function( $select, data )
    {
        $select.children('option').remove()

        $j.each(data, function(idx, element)
        {
            $select.append(
            $j("<option></option>")
                .attr("value", element.value)
                .html(element.text)
            )
        })
    }

    // http://stackoverflow.com/a/5341855
    // usage:
    // formatStringWithObject("i can speak {language} since i was {age}",{language:'javascript',age:10})
    // formatStringWithObject("i can speak {0} since i was {1}",'javascript',10})
    var formatStringWithObject = function (str, col)
    {
        col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1)

        return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function ( m, n ) {
            if (m == "{{") { return "{"; }
            if (m == "}}") { return "}"; }
    
            return col[n]
        })
    }

    // https://github.com/mouse0270/bootstrap-notify/blob/7b4711e091b8329cd15a2f7d59ea1cd5f8221ddf/bootstrap-notify.js#L56
    var formatStringArrayOnly = function ()
    {
        var args = arguments
        var str = arguments[0]
        
        return str.replace(/(\{\{\d\}\}|\{\d\})/g, function ( strMatch ) {
            if (strMatch.substring(0, 2) === "{{")
            {
                return strMatch
            }
            var num = parseInt(strMatch.match(/\d/)[0])
            
            return args[num + 1]
        })
    }

    // example usage:
    // var queryData = ZUtil.parseURLQuery(window.location.search.substring(1))
    var parseQueryStringAsArray = function( queryString ) {
        // remove leading '?', if any
        var keyvalues = queryString.replace(/^\?/, '').split('&')

        // if there are no parameters in query string,
        // we'll get empty string as first element
        if(keyvalues[0] == "") {
            return []
        }

        return keyvalues.map(function(element, idx) {
            var kv = element.split('=')
            return {
                name: decodeURIComponent(kv[0]),
                // '+' used as a ' ' in URI encoded strings is no being
                // decoded by decodeURIComponent() so we handle it here
                // It must be done *before* we call decodeURIComponent()
                // otherwise we'll replace %2B's with space
                value: decodeURIComponent(kv[1].replace(/\+/g, ' '))
            }
        })
    }

    var parseQueryStringAsObject = function( queryString ) {
        // remove leading '?', if any
        var keyvalues = queryString.replace(/^\?/, '').split('&')

        // if there are no parameters in query string,
        // we'll get empty string as first element
        if(keyvalues[0] == "") {
            return {}
        }

        var result = {}

        keyvalues.map(function(element, idx) {
            var kv = element.split('=')
            var name = decodeURIComponent(kv[0])
            // '+' used as a ' ' in URI encoded strings is no being
            // decoded by decodeURIComponent() so we handle it here
            // It must be done *before* we call decodeURIComponent()
            // otherwise we'll replace %2B's with space
            var value = decodeURIComponent(kv[1].replace(/\+/g, ' '))

            result[name] = value
        })

        return result
    }

    var isInsideElectron = function()
    {
        return !!(window && window.process && window.process.versions && window.process.versions.electron)
    }

    var fragments = function(fileContent)
    {
        var lines = fileContent.split('\n')
        
        // fragment starts with line:
        // #! <fragment_name>
        var fragmentNameRegex = /^#!\s(\w+)$/gm
        // a line inside the fragment which starts with '#' will be ignored, i.e.:
        // # anything goes after '#'
        var fragmentCommentRegex = /^#[^!].*$/gm
        // fragment ends with line:
        // #!
        var fragmentEndRegex = /^#!\s*$/gm

        var fragmentArray = {}
        var currentFragmentName = null
        var currentFragment = ''
        
        $j.each(lines, function(idx, line){
            var fragmentNameMatches = fragmentNameRegex.exec(line)
            var fragmentCommentMatches = fragmentCommentRegex.exec(line)
            var fragmentEndMatches = fragmentEndRegex.exec(line)

            if(fragmentNameMatches !== null)
            {
                currentFragmentName = fragmentNameMatches[1]
                currentFragment = ''
            }
            else if(fragmentEndMatches !== null)
            {
                fragmentArray[currentFragmentName] = currentFragment
                currentFragmentName = null
                currentFragment = ''
            }
            else if(fragmentCommentMatches === null)
            {
                if(currentFragmentName !== null)
                {
                    currentFragment += line + '\n'
                }
            }

        })

        return fragmentArray
    }

    var removeLeadingSlash = function(text)
    {
        return text.replace(/^\/+/, '')
    }

    var ensureLeadingSlash = function(text)
    {
        return text.replace(/^\/*/, '/')
    }

    var removeTrailingSlash = function(text)
    {
        return text.replace(/\/+$/, '')
    }

    var ensureTrailingSlash = function(text)
    {
        return text.replace(/\/*$/, '/')
    }

    // initialize the module
    initialize(module.config())
    // public functions interface
    $j.extend(true, ZUtil, {
        isInsideElectron: isInsideElectron,

        populateSelect: populateSelectOptions,

        formatString: formatStringWithObject,

        parseUrlQueryAsArray: parseQueryStringAsArray,
        parseUrlQueryAsObject: parseQueryStringAsObject,
        parseFragments: fragments,

        removeLeadingSlash: removeLeadingSlash,
        ensureLeadingSlash: ensureLeadingSlash,
        removeTrailingSlash: removeTrailingSlash,
        ensureTrailingSlash: ensureTrailingSlash,
        noop: function(){},
    })
    // done
    return ZUtil

})
