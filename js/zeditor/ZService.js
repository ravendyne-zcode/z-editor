'use strict'

define(['module', 'jquery',
    'zeditor/ZUtil', 'zeditor/ZServiceWeb', 'zeditor/ZServiceElectron'
    ], function(module, $j, ZUtil, ZServiceWeb, ZServiceElectron){

    var self = this
    var ZService = {}
    self.settings =
    {
        filesLocation: '/files',
        apiUrl: '/',
    }

    var initialize = function( config )
    {
        $j.extend(true, self.settings, config || {})

        if(ZUtil.isInsideElectron())
        {
            ZServiceElectron.extend(ZService, self.settings)
        }
        else
        {
            ZServiceWeb.extend(ZService, self.settings)
        }
    }

    var loadFile = function( fileName, loadFromFilesLocation = true )
    {
        return $j.Deferred().resolve('').promise()
    }

    var saveFile = function( fileName, content, saveToFilesLocation = true )
    {
        return $j.Deferred().resolve({}).promise()
    }

    var runCommand = function( cmd, terminalOutput )
    {
        return $j.Deferred().resolve({}).promise()
    }

    var buildProject = function( project, terminalOutput )
    {
        return $j.Deferred().resolve({}).promise()
    }

    // public functions interface
    $j.extend(true, ZService, {
        loadFile: loadFile,
        saveFile: saveFile,
        runCommand: runCommand,
        buildProject: buildProject,
    })
    initialize(module.config())

    return ZService

})
