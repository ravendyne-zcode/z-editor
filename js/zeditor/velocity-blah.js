/*
    - hides a button
    - creates it's clone in the middle of screen
    - blinks it couple of times
    - animates it from middle screen to original button position
    - destroys the clone
    - shows original button
*/
var animateButton = function(){
    var windowWidth = window.innerWidth,
    windowHeight = window.innerHeight,
    pageMidX = windowWidth/2,
    pageMidY = windowHeight/2;

    var $btnNew = $j('#btnNew')
    var btnOffset = $btnNew.offset()
    console.log('btnOffset', btnOffset)
    console.log($(window).scrollTop())
    console.log($(window).scrollLeft())
    
    var $button = $btnNew.clone()
    $button.css({
        position: 'fixed',
        left: pageMidX,
        top: pageMidY,
        transform: 'scale(3)',
        zIndex: 1000,
    })
    $button.appendTo('body')

    $btnNew.css({
        visibility: 'hidden',
    })

    $button
    .delay(100)
    // this doesn't work without forcefeeding initial scale value (3)
    .velocity({ scale: [1, 3] }, { duration: 100, delay: 50, easing: [0.0, 1.0, 0.8, 1.0] })
    // .velocity({ opacity: 0 }, { duration: 100, delay: 50 })
    // .velocity({ opacity: 1 }, { duration: 100, delay: 50 })
    .velocity({ opacity: 0 }, { duration: 100, delay: 50 })
    .velocity({ opacity: 1 }, { duration: 100, delay: 50 })
    // .velocity({ opacity: 0 }, { duration: 100, delay: 50 })
    // .velocity({ opacity: 1 }, { duration: 150 })
    // .velocity({ opacity: 0 }, { duration: 200, delay: 50 })
    // .velocity({ opacity: 1 }, { duration: 250 })
    .velocity({
        top: btnOffset.top,
        left: btnOffset.left,
        scale: 1,
    },{
        easing: [0.0, 1.0, 0.2, 1.0],
        complete: function(elements)
        {
            // console.log(elements);
            $button.css({
                visibility: 'hidden',
            })
            $btnNew.css({
                visibility: 'visible',
            })
            $button.remove()
        }
    })
}
