// $j = jQuery.noConflict()
// $ = $j

// http://requirejs.org/docs/jquery.html#noconflictmap
// This won't work though, since bootstrap and and some others
// need global jQuery to be present
/*
    map:
    {
        // http://requirejs.org/docs/jquery.html#noconflictmap
        // '*' means all modules will get 'myjquery'
        // for their 'jquery' dependency.
        '*': { 'jquery': 'myjquery' },
        // 'myjquery' wants the real jQuery module
        // though. If this line was not here, there would
        // be an unresolvable cyclic dependency.
        'myjquery': { 'jquery': 'jquery' }
    },
*/

define(['jquery'], function ($j) {
    return $j.noConflict( true )
})


