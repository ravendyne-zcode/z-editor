#!/usr/bin/env node

console.log('hello')

var exec = require('child_process').exec

var result = ''

var child = exec('ping -n 3 google.com')

child.stdout.on('data', function(data) {
    console.log(data)
    result += data
})

child.stderr.on('data', function(data) {
    console.log(data)
    // result += data
})

child.on('close', function() {
    console.log('done')
    console.log(result)
})
