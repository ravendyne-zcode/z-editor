volatile unsigned int * const UART0DR = (unsigned int *)0x101f1000;
 
void print_uart0(const char *s) {
 while(*s != '\0') { /* Loop until end of string */
 *UART0DR = (unsigned int)(*s); /* Transmit char */
 s++; /* Next char */
 }
}
 
void c_entry() {
 print_uart0("Hello world!\n");
}

/*

https://github.com/qemu/qemu/blob/master/hw/char/pl011.c
UART write, just put byte to UART register 0:
https://github.com/qemu/qemu/blob/master/hw/char/pl011.c#L166
UART read, just read byte from UART register 0:
https://github.com/qemu/qemu/blob/master/hw/char/pl011.c#L74



Stellaris LM3S6965 (Cortex-M3):
https://github.com/qemu/qemu/blob/master/hw/arm/stellaris.c#L1224

UART0 DR @ 4000c000

-M lm3s6965evb -m cortex-m3


GPIO uses PL061 device and does nothing right now.
State change could be observed on stdout if -DDEBUG_PL061 is defined on configure.

Hooking into code:
https://github.com/qemu/qemu/blob/master/hw/gpio/pl061.c#L115
might be able to read/write gpio from host.


*/