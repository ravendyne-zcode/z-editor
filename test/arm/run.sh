#!/bin/bash

# The -M option specifies the emulated system.
# The program prints “Hello world!” in the terminal and runs indefinitely; to exit QEMU, press “Ctrl + a” and then “x”.

qemu-system-arm -M versatilepb -m 128M -nographic -kernel test.bin

# qemu-system-arm -machine virt -cpu cortex-m3 -nographic -m 513 -kernel dso-lib.elf

