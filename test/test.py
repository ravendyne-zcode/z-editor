#!/usr/bin/env python

import sys
import ConfigParser

config = ConfigParser.SafeConfigParser()
config.read('test.cfg')

print config.get('section', 'foo')
print config.get('section', 'baz', vars={'baz': 'sure'})
print config.items('section')
